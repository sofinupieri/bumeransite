
import Image from 'next/image'
import styles from './page.module.css'
import Introduction from './pages/Introduction'
import { theme } from '@fravega-it/bumeran-ds-fvg';


import { ThemeProvider } from 'styled-components';
import ChipGroupDocumentation from './components/ChipGroup/ChipGroupDocumentation';
import LabelDocumentation from './components/ChipGroup/LabelDocumentation';
import LinkDocumentation from './components/ChipGroup/LinkDocumentation';
import ButtonDocumentation from './components/ChipGroup/ButtonDocumentation';



export default function Home() {
  return (
    <ThemeProvider theme={theme}>

    <main className={styles.main}>
      <div className={styles.description}>
        <Introduction />
      </div>

      

        <ChipGroupDocumentation />
        <LabelDocumentation />
        <LinkDocumentation />
        <ButtonDocumentation />




    </main>
    </ThemeProvider>
  )
}
 