import Image from "next/image";

export default function Banner() {
  return (
    <div>
      <Image
        src={"/images/banner.png"}
        width={0}
        height={0}
        sizes="100vw"
        style={{ width: "100vw", height: "auto" }}
      ></Image>
    </div>
  );
}
