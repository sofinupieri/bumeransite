import { styled } from "styled-components"
import Banner from "./Banner/Banner";

const PageWrapper = styled.div`
    height: max-content;
    margin: 50px 0;
`;

export default function Page({children}) {

    return (
        <PageWrapper>
            <Banner />
            {children}

        </PageWrapper>
    )
}