import { Button, Heading, TextBody } from "@fravega-it/bumeran-ds-fvg";

import styled from "styled-components";

const LastVersionContainer = styled.div`
  background-color: ${({ theme }) => theme.colors.green[100]};
  padding: ${({ theme }) => theme.spacing.m};
  border-radius: ${({ theme }) => theme.borderRadius.m};
`;

const ButtonsContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: ${({ theme }) => theme.spacing.s};
`;

export default function Introduction() {
  return (
    <>
      <div>
        <Heading size="xl"> Bienvenido a Búmeran</Heading>

        <LastVersionContainer>
          <Heading size="l"> Ultima versión V1.2.3</Heading>
          <Heading> ¿Qué cambió? </Heading>
          <TextBody>
            - Crear token de border-radius: 12px <br />
            - Reemplazar tablas de Storybook por tablas de Búmeran <br />
            - Outside click en Modal <br />
            - Agregar prop inputMode a TextInput
            <br />
            - Implementar solucion de ZIndex
            <br />
          </TextBody>
        </LastVersionContainer>

        <ButtonsContainer>
          <Button label="Guias de diseño " />
          <Button label="Ver documentación de desarrollo " />
          <Button
            as="link"
            href="https://bumeran-ds.fravega.com/"
            label="Soporte"
          />

          <Button label="Trabajá con nosotros" />
        </ButtonsContainer>
      </div>
    </>
  );
}
