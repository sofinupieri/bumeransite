

import { useCallback, useState } from "react";

import { ChipGroup } from "@fravega-it/bumeran-ds-fvg";
import Page from "@/app/DocumentationComponents/Page";


 const chipMock = [
    
    { id: 1, label: "Chip 1" },
    { id: 2, label: "Chip 2" },
    { id: 3, label: "Chip 3" }
  ];


export default function ChipGroupDocumentation() {
  const [chips, setChips] = useState(chipMock);

  console.log(chipMock)
  const onRemove = useCallback(
    (id) => {
      const filtered = chips.filter((chip) => chip.id !== id);

      setChips(filtered);
    },
    [chips, setChips]
  );
  return (
    <Page>
      <ChipGroup options={chips} size="s" onRemove={onRemove} />

      <ChipGroup options={chips} size="m" onRemove={onRemove} />

      <ChipGroup options={chips} disabled />


    </Page>
  );
}
