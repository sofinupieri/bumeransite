import Page from "@/app/DocumentationComponents/Page";
import { Button, Heading } from "@fravega-it/bumeran-ds-fvg";

export default function ButtonDocumentation() {
  return (
    <Page>
      <Heading> Variant </Heading>
      <Button label="Primary" variant="primary" />
      <Button label="Secondary" variant="secondary" />
      <Button label="Tertiary" variant="tertiary" />
      <Button label="Danger" variant="danger" />

      <Heading> Disabled </Heading>
      <Button label="Disabled" disabled />

      <Heading> Size </Heading>
      <Button label="Small" size="s" />
      <Button label="Medium" size="m" />

      <Heading> FullWidth </Heading>
      <Button label="Full Width" fullWidth />

      <Heading> Loading </Heading>

      <Button label="Loading" loading />
    </Page>
  );
}
