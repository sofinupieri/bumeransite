import { Heading, Label, NotificationIcon } from "@fravega-it/bumeran-ds-fvg";
import Page from "@/app/DocumentationComponents/Page";

export default function LabelDocumentation() {
  return (
    <Page>
      <Label label="Label" />

      {/* import { Label } from '@fravega-it/bumeran-ds-fvg' */}

      <div>
        <Heading> Color </Heading>
        <Label label="Neutral" />
        <Label color="aqua" label="Aqua" />
        <Label color="pink" label="Pink" />
        <Label color="blue" label="Blue" />
        <Label color="green" label="Green" />
        <Label color="yellow" label="Yellow" />
        <Label color="red" label="Red" />
      </div>

      <Heading> Size </Heading>
      <Label size="s" label="Label" />
      <Label size="m" label="Label" />

      <Heading> leftIcon </Heading>
      <Label leftIcon={<NotificationIcon />} label="Con ícono" />
    </Page>
  );
}
