import {
  Heading,
  Label,
  Link,
  NotificationIcon,
  TextBody,
} from "@fravega-it/bumeran-ds-fvg";
import Page from "@/app/DocumentationComponents/Page";

export default function LinkDocumentation() {
  return (
    <Page>
      <Heading> Variantes </Heading>
      <Link variant="action">
        <a href="#">Link con variante action</a>
      </Link>

      <Link variant="standalone">
        <a href="#">Link con variante standalone</a>
      </Link>

      <Link variant="danger">
        <a href="#">Link con variante danger</a>
      </Link>

      <TextBody>
        Lorem ipsum dolor sit amet,
        <Link variant="inline">
          <a href="#">Link con variante inline</a>
        </Link>
        consectetur adipiscing elit.
      </TextBody>

      <Heading> Disabled </Heading>

      <Link variant="action" disabled>
        <a href="#">Link disabled</a>
      </Link>

      <Heading> Tamaños </Heading>

      <Link size="s">
        <a href="#">Link de tamaño 's'</a>
      </Link>
      <Link size="m">
        <a href="#">Link de tamaño 'm'</a>
      </Link>

      <Heading> Iconos </Heading>
      <Link leftIcon={<NotificationIcon />}>
        <a href="#">Link con ícono a la izquierda</a>
      </Link>
      <Link rightIcon={<NotificationIcon />}>
        <a href="#">Link con ícono a la derecha</a>
      </Link>
    </Page>
  );
}
